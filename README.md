# KPhotoAlbum

This is the Website of KPhotoAlbum.org

Build instruction

```
gem install --user-install bundler jekyll
bundle config set --local path vendor/bundle
bundle install
```

## Run development

```
bundle exec jekyll serve
```
To see news items in the `_drafts` folder, add the `--draft` option.

### Check for broken links
Assuming you have [linkchecker](https://linkchecker.github.io/linkchecker/) installed, you can run the following command:

```
linkchecker http://127.0.0.1:4000/ --ignore-url=/news/.* --ignore-url=/media/images/trademark_kde_gear_black_logo.png
```

### Update dependencies

The dependencies are locked by `Gemfile.lock`. To update to the newest version, do the following:

```
rm Gemfile.lock
bundle update
```

## Run production

```
bundle exec jekyll build
```

## License:

* Content (text, image) are licensed under CC-BY-SA-4.0
* Code is licensed under AGPL-3.0-or-later

See LICENSES folder and [KDE licensing policy](https://community.kde.org/Policies/Licensing_Policy) for more information. 
