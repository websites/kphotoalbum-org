- name: Johannes Zarl-Zierl aka jzarl
  location: Linz, Austria
  areas: Maintainer (since 2019), Development, bug fixes, bug triage
  since: 2012
  image: /assets/img/developers/johannes.jpg
  active: true
  description: >
    I've been using KPhotoAlbum since around 2005. At that time I was doing mostly analog photography, which made KPhotoAlbum with its support for fuzzy dates an obvious choice - a choice that I am still happy with, today.

- name: Tobias Leupold aka l3u
  location: Konradsreuth, Germany
  areas: Development, bug fixes, releases
  since: April 2014
  image: /assets/img/developers/tobias.jpg
  active: true
  homepage: https://nasauber.de/
  description: >
    I started my "KPA carreer" because I thought that it was lacking the possibility to mark areas on the images and associate them with tags. With very much help of Johannes (who acted as a C++, Qt and KPA mentor for a long time and still does), I implemented this feature and worked on face detection and face recognition via libkface later.

    My goal is to improve KPA with new features and fix bugs wherever I can (sadly, after all, I'm not really a C++ pro yet, and probably never will ;-). I want to contribute especially to the face detection and recognition parts and the map bindings, speaking of geotagging and querying the database by geographic coordinates.

    In real life, I'm a dentist (yeah, I'm the only one here not making his bread with coding!), married to my lovely wife Barbara and dad of a son and a daughter.

- name: Robert Krawitz
  location: Brookline, MA, USA
  areas: Development, Optimization
  since: October 2018
  image: /assets/img/developers/robert.jpg
  active: true
  description: >
    I have been active in the FOSS world for over three decades, starting with work on X10 support for Emacs in the mid 1980's. I have worked in the computer systems industry since I graduated college in the late 1980's.

    I'm an avid photographer. In the late 1990's, looking for a more economical way of creating large format prints, I bought a photo printer from a friend, found no Linux driver for it but did find Mike Sweet's print plugin for GIMP, from which I started the project that eventually became Gutenprint. Almost 20 years later, I'm still project lead for Gutenprint.

    I'm a prolific photographer as well as an avid one. I photograph a lot of sporting events for my alma mater, and other events, and as a result have (currently!) about 275,000 frames totaling the better part of 3 TB. To manage those photos, I needed to find a good image management tool, and after trying several, settled on KimDaBa, which has since become KPhotoAlbum. I like the fact that it's fast and does not modify image files.

    I have been a confirmed performance geek as far back in my computing career as I recall. Performance and scalability are essential for any kind of professionally-oriented workflow. As such, most of my contributions to KPhotoAlbum have been in that area.

    Outside of work and photography (and the non-empty intersection of the two), my wife Manny and I enjoy the company of our little American Eskimo, Chloe.  

- name: Jesper K. Pederson aka blackie
  location: Hjorring, Denmark
  areas: Founder and former maintainer
  since: December 2002
  image: /assets/img/hall_of_fame/jesper.jpg
  active: true
  description: >
    Jesper was the main author and initiator of KPhotoAlbum. He started working on KPhotoAlbum when he got his first digital camera, as there simply was no other applications out there that could help him sort through all his images.

    When he is not working on KPhotoAlbum, he is enjoying life with his lovely wife Anne Helene, his two dogs Gunvald Larsson and Lena Kligstrom.

