#!/bin/bash

current_version=$(git --no-pager tag | sort --version-sort | tail -1)
last_version=$(git --no-pager tag | sort --version-sort | tail -2 | head -1)

echo "Authors between $last_version and $current_version:"
echo

git --no-pager log --pretty=format:%an $last_version..$current_version \
    | grep -v "l10n daemon script" \
    | sort -u -t " " -k 2
