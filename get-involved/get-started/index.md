---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

# Get Involved!

You like KPhotoAlbum and want to make it even better? We welcome any contribution anyone may bring.

### Where to start
The [community wiki](https://community.kde.org/Get_Involved/development) provides excellent resources for setting up your very own development environment.
It also contains [build instructions](https://community.kde.org/KPhotoAlbum/build_instructions) for KPhotoAlbum.

### Not a Programmer?

There are lots of ways you can support us! The KDE community wiki has a good page about how you can [get involved](https://community.kde.org/Get_Involved).
We are especially searching for people helping us with documentation, but any help is appreciated.

If you know how you could contribute but are unsure how to start, please get in touch with us and we'll set you up with the right people…

Even if you did not find an activity on how to support us, you can still talk about KPhotoAlbum: tell us and others why you like it…

### Get in Touch!

If you have any questions or ideas, and don't hesitate to [contact us](/user-support/communication/)!

