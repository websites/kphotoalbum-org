---
layout: page
title: First Step as a KPhotoAlbum Developer
---

# Join the team

## First Step as a KPhotoAlbum Developer

Once you have decided to participate in the development of KPhotoAlbum, you need to get your hands on the sources and build KPhotoAlbum yourself..

Please read the <a href="https://community.kde.org/KPhotoAlbum/build_instructions">build instructions</a> to learn how to compile the sources.

## Submitting a patch

When you have made a change, please mail a patches to the <a href="/user-support/communication/">mailing list</a>.
We also accept pull requests on <a href="https://invent.kde.org/graphics/kphotoalbum">KDE Invent</a>.

If you need help or would like to brainstorm your ideas, please ask on our mailing list.

### Extra credit

We use clang-format to format our code. If possible, please use our git hooks to prevent formatting problems with your code:

```
git config core.hooksPath ./dev/githooks/
```

## Getting git write access

Once you have supplied a number of patches, it is time to get your own git account. While sending patches is a great way for you to align up with coding standards and ways of doing things in KPhotoAlbum, it is not effective on a larger scale.

Read <a href="https://community.kde.org/Sysadmin/GitKdeOrgManual#How_to_get_read-write_developer_access">How to get read-write developer access</a>.
