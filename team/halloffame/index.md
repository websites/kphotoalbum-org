---
layout: page
title: Hall of Fame
sorted: 7
---

# Hall of Fame

People we owe special thanks to…

{% for dev in site.data.hall_of_fame %}
<div class="d-flex">
    <div class="flex-fill mb-2">
        <h2 class="mt-0">{{ dev.name }}</h2>
        <strong>Located:</strong> {{ dev.location }}<br />
        {% if dev.homepage %}
        <strong>Homepage:</strong> <a href="{{dev.homepage}}">{{dev.homepage}}</a><br />
        {% endif %}
        <strong>Area of responsability:</strong> {{ dev.areas }}<br />
        <strong>Since:</strong> {{ dev.since }}<br />
    </div>
    {% if dev.image %}
    <div>
        <img src="{{ dev.image }}" alt="Photo of {{ dev.name }}" />
    </div>
    {% endif %}
</div>
{{dev.description | markdownify}}
<hr class="mb-4" />
{% endfor %}

 - [Active Developers](/team)
