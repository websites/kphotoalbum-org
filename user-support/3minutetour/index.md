---
layout: page
title: 3 Minute Tour 
css-include: /css/main.css
---

# 3 Minute Tour

The main feature of KPhotoAlbum is its browser. It allows you to browse through the annotations you've made on your images. The default categories in KPhotoAlbum are Event, Places, People and Token. If these categories do not match your needs, you can add and remove ca
egories in Settings &rarr; Categories.

<figure class="text-center mt-3 mb-3">
<img src="/assets/img/documentation/browser-overview.png" class="img-fluid w-50" alt="Welcome screen of KPhotoAlbum" />
<figcaption>Welcome screen</figcaption>
</figure>

In the browser, you may choose a tag from a category, this will show all the items containing that tag. When you've chosen a tag from a category, the main page of the browser is shown again, this time, however, in scope of your current selection, meaning you will only 
ee information about those images matching the tag chosen in the category. In the screen shot, you can see the category page for the People category.

<figure class="text-center mt-3 mb-3">
<img src="/assets/img/documentation/persons.png" class="img-fluid w-50" alt="Persons list" />
<figcaption>List of persons in the database</figcaption>
</figure>

In the screenshot below, you see the result of choosing the person Jesper from the image database. First, notice that you actually see the current scope in the status bar. Next, notice how the counts for categories and images has changed, as a result of us now being in the scope of Jesper (i.e. only viewing images with Jesper on).

<figure class="text-center mt-3 mb-3">
<img src="/assets/img/documentation/jesper-overview.png" class="img-fluid w-50" alt="Browser Overview" />
<figcaption>Jesper Overview</figcaption>
</figure>

Besides browsing based on categories you can use the date bar at the bottom of the screen to limit the scope to a certain date range. To do so, click or drag the mouse below the bar graphs of the date bar.

<figure class="text-center mt-3 mb-3">
<img src="/assets/img/documentation/date.png" class="img-fluid w-50" alt="Browsing by date range" />
<figcaption>Browsing by date range</figcaption>
</figure>

To be able to browse based on tags, you have to tell KPhotoAlbum about those tags first. You can do so from the annotation dialog, which is shown below. In the annotation dialog, you may set properties for a single image at a time, or for a set of images. The dialog has been optimized for easy usage, so often it is possible to annotate more than 100 images in 10 minutes. An example of this optimization are the line edits above the list boxes. In those line edits, typing will be completed them from the list box. You can see that in the Location line edit where I've so far only typed an E.

<figure class="text-center mt-3 mb-3">
<img src="/assets/img/documentation/tagging.png" class="img-fluid w-50" alt="The image configuration dialog" />
<figcaption>The image configuration dialog</figcaption>
</figure>

Once you have annotated all your images, you may start your journey down memory lane. When you have narrowed your view to a set of images you wish to see, simply select view images in the browser window. This will bring up the thumbnail view which you may see on the left. From the thumbnail view you may select a set of images, and choose view images from the menu bar, or simply click on an image to view it.

<figure class="text-center mt-3 mb-3">
<img src="/assets/img/documentation/jesper-helene-new-york.png" class="img-fluid w-50" alt="Browser Overview" />
<figcaption>Jesper with Anne Hellen in New York</figcaption>
</figure>
