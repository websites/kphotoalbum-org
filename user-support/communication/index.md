---
layout: page
title: Get in Touch
sorted: 2
---

# Communication Channels

Communication in the KPhotoAlbum community is happening over the following media:

+ [Chat](https://webchat.kde.org/#/room/#kphotoalbum:kde.org)

+ [Mailling List](http://mail.kde.org/cgi-bin/mailman/listinfo/kphotoalbum/)

+ [Bugzilla](https://bugs.kde.org/enter_bug.cgi?product=kphotoalbum)


## Chat

If you need help with KPhotoAlbum you can reach us on the [#kphotoalbum](https://webchat.kde.org/#/room/#kphotoalbum:kde.org) matrix channel.
Feel free to ask any questions related to KPhotoAlbum or image management, or just hang out with us. The channel is low volume and is covered by the [KDE Code of Conduct](https://kde.org/code-of-conduct/).

The matrix channel is bridged to the [#kphotoalbum](ircs://libera.chat/kphotoalbum) channel on the Libera IRC network.


## Mailing List

Our mailing list can be found at [mail.kde.org](https://mail.kde.org/cgi-bin/mailman/listinfo/kphotoalbum/). It's used for all kind of communication, discussion, questions and so on.

The discussion lists are archived so it might be useful to read what has happened in the past.

## Bugzilla - The KDE Bug tracker

Bugs, inconveniences, and feature wishes are all entered into bugzilla.

To enter a bug in bugzilla, you may either go through the [wizard](https://bugs.kde.org/) or go directly to the [KPhotoAlbum Bug Entry](https://bugs.kde.org/enter_bug.cgi?product=kphotoalbum). The later requires that you have an account but following the URL allows you to create an account right away.

Follow this link to see the [list of open issues](https://bugs.kde.org/buglist.cgi?quicksearch=kphotoalbum)

