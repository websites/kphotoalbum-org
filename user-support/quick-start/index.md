---
layout: page
title: Documentation
css-include: /css/main.css
sorted: 1
---

# Quick start

There are many great features. Many are really straightforward.

The good starting point is to look at [this fast overview](/user-support/3minutetour/) and to watch these videos. A [PDF version](http://docs.kde.org/development/en/extragear-graphics/kphotoalbum/kphotoalbum.pdf) of the KDE style documentation is available as well as an [on-line version](http://docs.kde.org/development/en/extragear-graphics/kphotoalbum/index.html). Once you have KPhotoAlbum running you should be able to access the documentation under the Help menu using KDE Help Center.

The documentation can always be improved. If you wish to help us don't hesitate to [contact us](/user-support/communication/).
